#!/bin/bash
echo "Downloading stuff"

mkdir $HOME/mac-mod
cd $HOME/mac-mod

wget -O wallpapers.zip http://drive.noobslab.com/data/Mac-13.10/MBuntu-Wallpapers.zip
wget -O mac-fonts.zip http://drive.noobslab.com/data/Mac-14.04/macfonts.zip
wget -O mac-docky.zip http://drive.noobslab.com/data/Mac-14.04/Mac-OS-Lion%28Docky%29.tarmacfonts.zip

echo "Adding repo's"

sudo add-apt-repository ppa:docky-core/ppa
sudo add-apt-repository ppa:noobslab/themes
sudo add-apt-repository ppa:noobslab/apps


echo "Updating your repo's"
sudo apt-get update
sudo apt-get upgrade

echo "Installing new theme's and mods"
sudo apt-get install -y docky mac-ithemes-v3 mac-icons-v3 mbuntu-bscreen-v3 mbuntu-lightdm-v3 indicator-synapse unity-tweak-tool libreoffice-style-sifr wine p7zip-full

echo "Replace 'Ubuntu Desktop' text with 'Mac' on the Panel"
cd && wget -O Mac.po http://drive.noobslab.com/data/Mac-14.04/change-name-on-panel/mac.po
cd /usr/share/locale/en/LC_MESSAGES; sudo msgfmt -o unity.mo ~/Mac.po;rm ~/Mac.po;cd

echo "Disable overlay scrollbar"
gsettings set com.canonical.desktop.interface scrollbar-mode normal

echo "Remove White Dots and Ubuntu Logo from Lock Screen"
sudo xhost +SI:localuser:lightdm
sudo su lightdm -s /bin/bash
gsettings set com.canonical.unity-greeter draw-grid false;exit
sudo mv /usr/share/unity-greeter/logo.png /usr/share/unity-greeter/logo.png.backup

echo "Apple Logo in Launcher"
wget -O launcher_bfb.png http://drive.noobslab.com/data/Mac-14.04/launcher-logo/apple/launcher_bfb.png
sudo mv launcher_bfb.png /usr/share/unity/icons/

echo "Install mac fonts"
sudo unzip mac-fonts.zip -d /usr/share/fonts; rm mac-fonts.zip
sudo fc-cache -f -v

echo "More info => http://www.noobslab.com/2014/04/macbuntu-1404-pack-is-released.html" 

